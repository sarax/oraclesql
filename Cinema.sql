DROP TABLE FRole;
DROP TABLE MES;
DROP TABLE Film;
DROP TABLE Personne;

CREATE TABLE Personne(
    Pid INTEGER PRIMARY KEY,
    Prenom CHAR(20) NOT NULL,
    Nom CHAR(20) NOT NULL
);

CREATE TABLE Film(
    Fid INTEGER PRIMARY KEY,
    Titre VARCHAR(60) NOT NULL,
    An INTEGER NOT NULL  Constraint check_annee CHECK ( An > 1900),
    Dur INTEGER NOT NULL,
    Rang INTEGER NOT NULL
);

CREATE TABLE FRole(
    Fid INTEGER NOT NULL,
    Pid INTEGER NOT NULL,
    Nom CHAR(30) NOT NULL,
    PRIMARY KEY(Fid, Pid, Nom),
    CONSTRAINT FilmRole FOREIGN KEY(Fid) REFERENCES Film(Fid),
    CONSTRAINT PersonneRole FOREIGN KEY(Pid) REFERENCES Personne(Pid)

);

CREATE TABLE MES(
    Fid INTEGER NOT NULL,
    Pid INTEGER NOT NULL,
    PRIMARY KEY(Fid, Pid),
    FOREIGN KEY(Fid) REFERENCES Film(Fid),
    FOREIGN KEY(Pid) REFERENCES Personne(Pid)
);


-- Supprimer la contrainte identifiée
ALTER TABLE Film DROP CONSTRAINT check_annee;
